module.exports = {
    collectCoverageFrom: ['src/**/*.{ts}', '!src/context/*.{ts}'],
    globals: {
        'ts-jest': {
            diagnostics: false,
        },
    },
    moduleDirectories: ['node_modules', 'src'],
    moduleNameMapper: {
        '^@lib/(.*)$': '<rootDir>/src/$1',
    },
    preset: 'ts-jest',
    rootDir: '.',
    testMatch: ['**/?(*.)+(spec).[t]s?(x)'],
    verbose: true,
};
