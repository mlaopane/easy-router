import express from 'express';
import { Route, RoutesPool } from './Route';

export interface Router {
    readonly expressRouter: express.Router;
    readonly routes: Route[];
    plugRoute: (route: Route) => void;
    plugRoutesPool: (routesPool: RoutesPool) => void;
}
