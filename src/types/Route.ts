import { RequestHandler } from 'express';
import { Method } from './Method';

export interface Route {
    method: Method;
    middlewares?: RequestHandler[];
    path: string;
    handle: RequestHandler;
}

export interface RoutesPool {
    [routeName: string]: Route;
}
