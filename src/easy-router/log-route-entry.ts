import { Route } from '../types';

export default function logRouteEntry([routeName, route]: [string, Route]) {
    console.log(
        `> Plugging route ${routeName} (${route.method.toUpperCase()} ${
            route.path
        })`,
    );
}
