import { Router as ExpressRouter } from 'express';
import logRouteEntry from './log-route-entry';
import { makeCatchableAsyncHandler } from '../middlewares';
import { Route, Router, RoutesPool } from '../types';

class EasyRouter implements Router {
    public readonly expressRouter = ExpressRouter();

    public get routes(): Route[] {
        const layers = this.expressRouter.stack;

        return layers.map((layer) => {
            const { path, stack } = layer.route;
            const [{ handle, method }] = stack;
            return { handle, method, path };
        });
    }

    public plugRoute({ handle, method, middlewares = [], path }: Route) {
        this.expressRouter[method](
            path,
            ...middlewares,
            makeCatchableAsyncHandler(handle),
        );
    }

    public plugRoutesPool(routesPool: RoutesPool) {
        Object.entries(routesPool).forEach(([routeName, route]) => {
            this.plugRoute(route);

            if (process.env.NODE_ENV === 'development') {
                logRouteEntry([routeName, route]);
            }
        });
    }
}

export function createEasyRouter() {
    return new EasyRouter();
}
