import { Request, Response, NextFunction } from 'express';

export default function makeCatchableAsyncMiddleware(
    asyncFn: (req: Request, res: Response, next: NextFunction) => Promise<any>,
) {
    return (req: Request, res: Response, next: NextFunction) => {
        return asyncFn(req, res, next).catch(next);
    };
}
