const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
    externals: [nodeExternals()],
    mode: 'production',
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /(dist|examples|node_modules)/,
            },
        ],
    },
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'dist'),
    },
    resolve: {
        alias: {
            '@lib': path.resolve(__dirname, 'src'),
        },
        extensions: ['.ts'],
        mainFields: ['main'],
        modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    },
    target: 'node',
};
