import { makeCatchableAsyncHandler } from '../../src/middlewares';

describe(`Calling a failing async middleware with an error catcher`, function() {
    async function asyncMiddleware(req, res, next) {
        throw new Error('Oops !');
    }

    function errorCatcher(error) {
        return error.message;
    }

    describe(`Without making it catchable`, function() {
        it(`should throw an error`, async function() {
            expect.assertions(1);

            try {
                await asyncMiddleware(null, null, errorCatcher);
            } catch (error) {
                expect(error.message).toBe('Oops !');
            }
        });
    });

    describe(`Making it catchable`, function() {
        it(`should not throw`, async function() {
            expect.assertions(1);

            const catchableAsyncMiddleware = makeCatchableAsyncHandler(
                asyncMiddleware,
            );

            const result = await catchableAsyncMiddleware(
                null,
                null,
                errorCatcher,
            );

            expect(result).toBe('Oops !');
        });
    });
});
