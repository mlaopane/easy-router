import { createEasyRouter } from '@lib/easy-router';
import { Route, Method, RoutesPool } from '@lib/types';

describe(`Plugging a route to the router`, function() {
    const router = createEasyRouter();
    const route: Route = {
        handle: async () => {
            return 'Success';
        },
        method: Method.Post,
        path: '/my-path',
    };

    router.plugRoute(route);

    it(`should add the route to the internal express router`, async function() {
        expect.assertions(3);

        const [{ handle, method, path }] = router.routes;

        await expect(method).toEqual(Method.Post);
        await expect(path).toEqual('/my-path');
        await expect(await handle()).toEqual('Success');
    });
});

describe(`Plugging a routes pool to the router`, function() {
    const router = createEasyRouter();

    const routesPool: RoutesPool = {
        eatApples: {
            handle: async () => {
                return `Yummy !`;
            },
            method: Method.Post,
            path: `/eat-apples`,
        },
        eatBananas: {
            handle: async () => {
                return `Yummy !`;
            },
            method: Method.Post,
            path: `/eat-bananas`,
        },
    };

    router.plugRoutesPool(routesPool);

    it(`should add the routes to the internal express router`, async function() {
        expect.assertions(6);

        const [eatApples, eatBananas] = router.routes;

        await expect(eatApples.method).toEqual(Method.Post);
        await expect(eatApples.path).toEqual(`/eat-apples`);
        await expect(await eatApples.handle()).toEqual(`Yummy !`);

        await expect(eatBananas.method).toEqual(Method.Post);
        await expect(eatBananas.path).toEqual(`/eat-bananas`);
        await expect(await eatBananas.handle()).toEqual(`Yummy !`);
    });
});
