import express from 'express';
import easyRouter from './src/router';

const app = express();

app.use(easyRouter.expressRouter);

app.listen(3000, () => {
    console.log(`Development example available @ http://localhost:3000`);
});
