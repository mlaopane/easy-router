import { Route, Method } from '@mlaopane/express-easy-router';
import { Response, Request } from 'express';

async function eatPotatoes(req: Request, res: Response) {
    res.status(200).json({ message: 'I like potatoes <3' });
}

const eatPotatoesRoute: Route = {
    handle: eatPotatoes,
    method: Method.Post,
    path: '/eat-potatoes',
};

export default eatPotatoesRoute;
