import { createEasyRouter } from '@mlaopane/easy-router';
import eatPotatoesRoute from './routes/eat-potatoes';

const easyRouter = createEasyRouter();

easyRouter.plugRoute(eatPotatoesRoute);

export default easyRouter;
